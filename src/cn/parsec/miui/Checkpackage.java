package cn.parsec.miui;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;

public class Checkpackage {

	public static void main(String[] args) {
		HttpClient httpClient = new DefaultHttpClient();

		try {
			HttpPost request = new HttpPost(
					"http://policy.app.xiaomi.com/cms/interface/v1/checkpackages.php");
			StringEntity params = new StringEntity(
					"{\"packages\":[\"com.example.wanyougame\\/1\",\"com.qihoo.appstore\\/109017601\",\"parsec.appexpert\\/115\"]}");
			request.addHeader("content-type", "text/plain, charset=iso-8859-1");
			request.setEntity(params);
			HttpResponse response = httpClient.execute(request);

			StringBuilder sb = new StringBuilder();
			InputStream in = response.getEntity().getContent();
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(in));
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line);

			}

			System.out.println(sb.toString());
			// handle response here...
		} catch (Exception ex) {
			// handle exception here
		} finally {
			httpClient.getConnectionManager().shutdown();
		}
	}

	
	void test()
	{
	}
}
